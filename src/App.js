import Banner from "./components/Banner";
import Bottom from "./components/Bottom";
import Features from "./components/Features";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";

function App() {
  return (
    <>
        <Navbar />
        <Banner />
        <Features />
        <Bottom />
        <Footer dataSample="hello world" />
    </>
  );
}

export default App;
