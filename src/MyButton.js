import { Component } from "react";
import style from "./MyButton.module.css"

class MyButton extends Component {
    constructor(props) {
        super(props)

        // melakukan proses logic untuk prepare state
        this.state = {
            counter: 0
        }
    }

    handleClick = () => {
        this.setState({
            counter: this.state.counter + 1
        })
        console.log(this.state.counter)
    }

    render() {
        return (
        <button className={style.Tombol} onClick={this.handleClick}>
            {this.props.title}
        </button>
        )
    }
}

export default MyButton