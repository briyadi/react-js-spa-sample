import "./Hello.css"

function Hello(props) {
    return (
        <>
            <h1>{props.title}</h1>
            <p>Lorem ipsum dolor sit.</p>
            <button className="btn btn-primary">Click Me</button>
        </>
    )
}

export default Hello