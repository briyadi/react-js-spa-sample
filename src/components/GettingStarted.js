const style = {
    btnContainer: {
        paddingTop: 60,
    },
    btnLink: {
        color: "#61dafb",
        textDecoration: "none",
    },
    btnInfo: {
        borderRadius: 0,
        backgroundColor: "#61dafb",
    },
    btn: {
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 20,
        paddingRight: 20,
        fontSize: 20,
    }
}

function GettingStarted(props) {
    return (
        <div className="d-inline-block" style={style.btnContainer}>
            <a href="/#" className="btn btn-info" style={{...style.btnInfo, ...style.btn}}>Get Started</a>
            <a href="/#" className="btn btn-link" style={{...style.btnLink, ...style.btn}}>Take the Tutorial &rsaquo;</a>
        </div>
    )
}

export default GettingStarted