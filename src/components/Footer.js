import { useEffect, useState } from "react"

const style = {
    container: {
        color: "white",
        backgroundColor: "#20232a",
        minHeight: 400,
        paddingTop: 20,
    }
}

function Footer(props) {

    const [value, setValue] = useState(1)

    useEffect(() => {
        setTimeout(() => {
            setValue(2)
        }, 10000)
    }, [setValue])
    
    return (
        <div className="container-fluid" style={style.container}>
            <div className="row">
                <div className="col">
                    {value}
                </div>
                <div className="col">
                    <h4>DOCS</h4>
                    <p>Installation</p>
                    <p>Main Concepts</p>
                    <p>Advanced Guides</p>
                    <p>API Reference</p>
                    <p>Hooks</p>
                    <p>Testing</p>
                    <p>Contributing</p>
                    <p>FAQ</p>
                </div>
                <div className="col">
                    <h4>CHANNELS</h4>
                    <p>GitHub</p>
                    <p>Stack Overflow</p>
                    <p>Discussion Forums</p>
                    <p>Reactiflux Chat</p>
                    <p>DEV Community</p>
                    <p>Facebook</p>
                    <p>Twitter</p>
                </div>
            </div>
        </div>
    )
}

export default Footer