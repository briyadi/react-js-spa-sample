import GettingStarted from "./GettingStarted"

const style = {
    container: {
        backgroundColor: "#282c34",
        textAlign: "center",
        paddingTop: 0,
        paddingBottom: 30,
    }
}

function Bottom(props) {
    return (
        <div className="container-fluid" style={style.container}>
            <GettingStarted />
        </div>
    )
}

export default Bottom