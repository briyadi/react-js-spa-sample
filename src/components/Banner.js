import "./Banner.css"
import GettingStarted from "./GettingStarted"

const style = {
    container: {
        minHeight: 450,
        backgroundColor: "#282c34",
        textAlign: "center",
    },
    title: {
        color: "#61dafb",
        fontSize: 60,
        paddingTop: 80,
    },
    paragraph: {
        fontSize: 25,
        fontWeight: 100,
    }
}

function Banner(props) {
    return (
        <div className="container-fluid banner" style={style.container}>
            <h1 className="text-center" style={style.title}>React</h1>
            <p className="text-center text-white" style={style.paragraph}>A JavaScript library for building user interfaces</p>
            <GettingStarted />
        </div>
    )
}

export default Banner